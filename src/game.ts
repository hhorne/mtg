import Player from './player'

export default class Game implements IGame {
  private _activePlayerId: string = ''
  private _players = new Map<string, Player>()
  private _stack: any[] = []
  private _turnPhaseId: string = ''
  private _turnOrder: string[]

  get activePlayerId() { return this._activePlayerId }
  get stack() { return this._stack }
  get turnPhaseId() { return this._turnPhaseId }
  get turnOrder() { return this._turnOrder }

  constructor(players: Player[], turnOrder: string[]) {
    players.forEach(p => this._players.set(p.id, p))
    this._turnOrder = turnOrder
  }

  play() {
    
  }
}

export interface IGame {
  activePlayerId: string
  stack: any[]
  turnPhaseId: string
  turnOrder: string[]
}