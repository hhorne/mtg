import uuid from 'uuid/v4'

export default class Guid extends String {
  static create() {
    return uuid()
  }
}