import Component, { ComponentInitializer } from '../shared/component'

export default abstract class Entity {
	components: Map<string, Component> = new Map()

	constructor(components: ComponentInitializer[] = []) {
		this.registerComponents(components)
	}

	registerComponents(componentInitializers: ComponentInitializer[]) {
		componentInitializers
			.forEach(({ type, args = [] }) =>
				this.registerComponent(type, ...args)
			)
	}

	registerComponent(type: any, ...args: any[]): boolean {
		if (this.hasComponent(type.id)) {
			return false
		}

		this.components.set(type.id, new type(this, ...args))
		return true
	}

	hasComponent<T extends Component>(componentId: string): boolean {
		return this.components.has(componentId)
	}

	getComponent<T extends Component>(componentId: string): T {
		const result = this.components.get(componentId) as T
		if (result === undefined) {
			throw new Error('Component not found')
		}
		
		return result
	}
}