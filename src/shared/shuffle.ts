/**
 * Shuffles array in place. Fisher-Yates shuffleish?
 * @param {Array} collection	An array containing the items.
 */
export default (collection: any[]) => {
	var j, x, i;
	for (i = collection.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		x = collection[i];
		collection[i] = collection[j];
		collection[j] = x;
	}
	return collection;
}