enum Mana {
  White ='☀️',
  Red = '🔥',
  Black = '💀',
  Blue = '💧',
  Green = '🌳',
  Colorless = '◇'
}

export default Mana