import { Exile, Graveyard, Hand, Library } from './components'

export default {
  Exile: Exile.id,
  Graveyard: Graveyard.id,
  Hand: Hand.id,
  Library: Library.id,
}