import Zone from './zone'
import Card from '../../card';
import { createCards, createPlayer } from './shared/test-helpers'

class TestZone extends Zone {
  get id() { return 'TestZone' }
}

describe('Zone', () => {

	describe('addCards()', () => {

		it('adds cards to the card collection', () => {
			const player = createPlayer()
			const zone = new TestZone(player)
	
			zone.addCards(createCards(1))
	
			expect(zone.cards.length).toBe(1)
		})

	})

	describe('removeCards()', () => {

		it('removes cards from the card collection', () => {
			const cards = createCards(1)
			const player = createPlayer()
			const zone = new TestZone(player, cards)

			zone.removeCards(cards)

			expect(zone.cards.length).toBe(0)
		})

	})

})