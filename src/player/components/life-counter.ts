import Component from '../../shared/component'
import { IPlayer } from '../'
import * as Defaults from '../defaults'

export default class LifeCounter implements Component {
	static readonly id: string = 'LifeCounter'
	private readonly _player: IPlayer
	private _lifeCount: number
	private _damages: Array<number> = []
	private _totalDamage: number = 0
	private _gains: Array<number> = []
	private _totalGains: number = 0

	constructor(player: IPlayer, lifeCount: number = Defaults.LifeTotal) {
		this._player = player
		this._lifeCount = lifeCount
	}

	get id(): string {
		return this.id
	}

	get lifeCount(): number {
		return this._lifeCount + this._totalGains - this._totalDamage
	}

	get damages(): Array<number> {
		return this._damages
	}

	get totalDamage(): number {
		return this._totalDamage
	}

	get gains(): Array<number> {
		return this._gains
	}

	get totalGains(): number {
		return this._totalGains
	}

	takeDamage(damage: number) {
		if (damage < 0) {
			throw new Error('Damage value cannot be negative')
		}

		this._damages.push(damage)
		this._totalDamage = this._damages.reduce((prev,next) => prev + next, 0)
	}

	gainLife(life: number) {
		if (life < 0) {
			throw new Error('Life value cannot be negative')
		}

		this._gains.push(life)
		this._totalGains = this._gains.reduce((prev,next) => prev + next, 0)
	}
}