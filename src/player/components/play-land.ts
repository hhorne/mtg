import Component from "../../shared/component";
import * as Defaults from '../defaults'
import { IPlayer } from '../'
import { ICard } from '../../card'
import { IGame } from '../../game'
import { IZone } from './zone'

export default class PlayLand implements Component {
  static readonly id = 'PlayLand'
  private readonly _baseLimit: number = 0
  private _playedThisTurn: number = 0
  private readonly _player: IPlayer
  private readonly _modifiers: any[] = []
  private readonly _zoneSources: ZoneLandSource[] = [{ 
    sourceId: 'default',
    name: 'Hand',
    selector: (player) => player.hand,
  }]

  constructor(player: IPlayer, baseLimit: number = Defaults.LandPerTurn) {
    this._baseLimit = baseLimit
    this._player = player
  }

  get id(): string { return this.id }
  
  get limit() {
    return this._modifiers.reduce(
      (prev, next) => prev + next.value,
      this._baseLimit
    )
  }

  get playedThisTurn() {
    return this._playedThisTurn
  }

  get isUnderLimit() {
    return this.limit > this._playedThisTurn
  }

  get zones() {
    return this._zoneSources.map(z => z.selector(this._player))
  }

  canPlayLand(game: IGame) {
    const { activePlayerId, stack, turnPhaseId } = game
    const { id: playerId } = this._player
    return playerId === activePlayerId
      && stack.length === 0
      && turnPhaseId.startsWith('MainPhase')
      && this.isUnderLimit
  }

  getOptions(game: IGame) {
    const cards: ICard[] = []

    if (this.canPlayLand(game)) {
      cards.push(...this.zones
        .map(zone => zone.cards)
        .reduce((prev, next) => 
          prev.concat(next.filter(isLand)),
        [])
      )
    }

    return cards
  }
}

export interface ZoneLandSource {
  sourceId: string
  name: string
  selector(player: IPlayer): IZone
}

const isLand = (card: ICard) => card.types.includes('land')