import Zone from './zone'
import { IPlayer } from '../'
import { ICard } from '../../card'
import shuffle from '../../shared/shuffle'

export default class Library extends Zone {
	static readonly id: string = 'Library'

	constructor(player: IPlayer, cards: ICard[] = []) {
		super(player, cards)
	}

	get id(): string {
		return this.id
	}

	shuffle() {
		shuffle(this._cards)
	}

	draw(): ICard | undefined {
		return this._cards.shift()
	}
}