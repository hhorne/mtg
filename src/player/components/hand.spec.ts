import Hand from './hand'
import Card from '../../card'
import { createCards, createPlayer } from './shared/test-helpers'

describe('Hand', () => {

  it('is initialized empty', () => {
    const player = createPlayer()
    const hand = new Hand(player)
    const { cards } = hand
    
    const count = cards.length
    
    expect(count).toBe(0)
  })

  describe('add()', () => {

    it('puts cards into Hand.cards', () => {
      const player = createPlayer()
      const cards = createCards(1)
      const hand = new Hand(player)

      hand.addCards(cards)

      expect(hand.size).toBe(1)
    })

  })

  describe('discard()', () => {

    it('returns discarded cards after removing from hand', () => {
      const player = createPlayer()
      const hand = new Hand(player)
      hand.addCards([
        new Card({ name: 'cool card', types: [] }),
        new Card({ name: 'other card', types: [] }),
        new Card({ name: 'dumb card', types: [] }),
      ])

      const cardToDiscard = [hand.cards[0], hand.cards[1]]

      const discarded = hand.discard(cardToDiscard)

      expect(hand.size).toBe(1)
      expect(discarded.find(d => d === cardToDiscard[0])).toBeTruthy()
      expect(discarded.find(d => d === cardToDiscard[1])).toBeTruthy()
    })

  })

})