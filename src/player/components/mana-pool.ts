import Component from '../../shared/component'
import { IPlayer } from '../'
import Mana from '../../mana'

export default class ManaPool implements Component {
  static readonly id: string = 'ManaPool'
  private readonly _player: IPlayer
  private _mana: Mana[] = []

  constructor(player: IPlayer) {
    this._player = player
  }

  get id(): string {
    return this.id
  }

  get mana(): Mana[] {
    return this._mana
  }

  addMana(mana: Mana[]) {
    this._mana = this._mana.concat(mana)
  }

  useMana(mana: Mana[]): boolean {
    for (const point of mana) {
      const index = this._mana.findIndex(m => m === point)
      if (index < 0) {
        continue
      }

      this._mana.splice(index, 1)
      return true
    }

    return false
  }

  empty() {
    this._mana = []
  }
}
