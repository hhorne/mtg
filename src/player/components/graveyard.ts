import Zone from './zone'
import { IPlayer } from '../'
import { ICard } from '../../card'

export default class Graveyard extends Zone {
  static readonly id = 'Graveyard'

  get id(): string { return this.id }

  constructor(player: IPlayer, cards: ICard[] = []) {
    super(player, cards)
  }
}