import Zone from './zone'
import { IPlayer } from '../'
import { ICard } from '../../card'

export default class Hand extends Zone {
  static readonly id: string = 'Hand'

  constructor(player: IPlayer, cards: ICard[] = []) {
    super(player, cards)
  }

  get id(): string {
    return this.id
  }

  discard(cards: ICard[]): ICard[] {
    return this.removeCards(cards)
  }
}