import PlayLand from './play-land'
import { createPlayer } from './shared/test-helpers'
import Hand from './hand'
import * as Defaults from '../defaults'
import { ICard } from '../../card'

describe('PlayLand', () => {

  it('uses the default limit', () => {
    const player = createPlayer()
    const landPlayer = new PlayLand(player)

    const limit = landPlayer.limit

    expect(limit).toBe(Defaults.LandPerTurn)
  })

  it('starts under the limit', () => {
    const player = createPlayer()
    const landPlayer = new PlayLand(player)

    const isUnderLimit = landPlayer.isUnderLimit

    expect(landPlayer.playedThisTurn).toBe(0)
    expect(isUnderLimit).toBeTruthy()
  })

  describe('getOptions()', () => {

    const cards: ICard[] = [
      { id:'1', name: 'mountain', types: ['land'] },
      { id:'2', name: 'island', types: ['land'] },
      { id:'3', name: 'stone of stone', types: ['artifact'] },
      { id:'4', name: 'beastly boi', types: ['creature'] },
    ]

    it('returns lands from the default zone', () => {
      const player = createPlayer({ id: 'player1' })
      player.hand = new Hand(player, cards)
      const landPlayer = new PlayLand(player)

      const options = landPlayer.getOptions({
        activePlayerId: 'player1',
        stack: [],
        turnPhaseId: 'MainPhase',
        turnOrder: [],
      })

      expect(options.length).toBe(2)
    })
  })

})