import LifeCounter from './life-counter'
import * as Defaults from '../defaults'
import { createPlayer } from './shared/test-helpers'

describe('LifeCounter', () => {

	it('starts off with the default player life total', () => {
		const player = createPlayer()
		const lifeCounter = new LifeCounter(player)

		const lifeCount = lifeCounter.lifeCount

		expect(lifeCount).toBe(Defaults.LifeTotal)
	})

	it('can start with a custom value', () => {
		const player = createPlayer()
		const customLifeStart = Defaults.LifeTotal + 1
		const lifeCounter = new LifeCounter(player, customLifeStart)

		expect(lifeCounter.lifeCount).toBe(customLifeStart)
	})

	describe('takeDamage()', () => {

		it('reduces a players lifeCount by the amount of damage passed in', () => {
			const player = createPlayer()
			const lifeCounter = new LifeCounter(player)

			lifeCounter.takeDamage(1)
			lifeCounter.takeDamage(2)
			lifeCounter.takeDamage(3)

			expect(lifeCounter.lifeCount).toBe(Defaults.LifeTotal - 6)
			expect(lifeCounter.damages.length).toBe(3)
			expect(lifeCounter.totalDamage).toBe(6)
		})

		it('throws an error when passed negative values', () => {
			const player = createPlayer()
			const lifeCounter = new LifeCounter(player)

			expect(() => lifeCounter.takeDamage(-5))
				.toThrow('Damage value cannot be negative')
		})

	})

	describe('gainLife()', () => { 

		it('increases a players lifeCount by the amount of life passed in', () => {
			const player = createPlayer()
			const lifeCounter = new LifeCounter(player)

			lifeCounter.gainLife(1)
			lifeCounter.gainLife(2)
			lifeCounter.gainLife(3)

			expect(lifeCounter.lifeCount).toBe(Defaults.LifeTotal + 6)
			expect(lifeCounter.gains.length).toBe(3)
			expect(lifeCounter.totalGains).toBe(6)
		})

		it('throws an error when passed negative values', () => {
			const player = createPlayer()
			const lifeCounter = new LifeCounter(player)

			expect(() => lifeCounter.gainLife(-5))
				.toThrow('Life value cannot be negative')
		})
	})

})