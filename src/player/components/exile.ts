import Zone from './zone'
import { IPlayer } from '../'
import { ICard } from '../../card'

export default class Exile extends Zone {
  static readonly id = 'Exile'

  get id(): string { return this.id }

  constructor(player: IPlayer, cards: ICard[] = []) {
    super(player, cards)
  }
}