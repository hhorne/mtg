import ManaPool from './mana-pool'
import Mana from '../../mana'
import { createPlayer } from './shared/test-helpers';

describe('ManaPool', () => {

  it('pool defaults to empty', () => {
		const player = createPlayer()
    const manaPool = new ManaPool(player)

    const manaCount = manaPool.mana.length

    expect(manaCount).toBe(0)
  })

  describe('addMana()', () => {

    it('adds mana symbols to the pool', () => {
      const player = createPlayer()
      const manaPool = new ManaPool(player)

      manaPool.addMana([Mana.Colorless])

      expect(manaPool.mana.length).toBe(1)
      expect(manaPool.mana[0]).toBe(Mana.Colorless)
    })

  })

  describe('useMana()', () => {

    it('removes specified mana from the pool', () => {
      const player = createPlayer()
      const manaPool = new ManaPool(player)
      manaPool.addMana([Mana.Colorless, Mana.Red])
  
      const wasUsed = manaPool.useMana([Mana.Colorless])
  
      expect(wasUsed).toBe(true)
      expect(manaPool.mana.length).toBe(1)
      expect(manaPool.mana[0]).toBe(Mana.Red)
    })

  })

  describe('empty()', () => {

    it('clears all values from mana', () => {
      const player = createPlayer()
      const manaPool = new ManaPool(player)
      manaPool.addMana([Mana.Colorless, Mana.Red, Mana.Red, Mana.Red])
  
      manaPool.empty()
  
      expect(manaPool.mana.length).toBe(0)
    })

  })

})