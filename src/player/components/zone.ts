import Component from '../../shared/component'
import { ICard } from '../../card'
import { IPlayer } from '../'

export default abstract class Zone implements Component, IZone {
  protected _cards: ICard[]
  protected _player: IPlayer

  abstract get id(): string
  get cards() { return this._cards }
  get size() { return this._cards.length }
  
  constructor(player: IPlayer, cards: ICard[] = []) {
    this._cards = cards
    this._player = player
  }

  addCards(cards: ICard[]) {
    const incomingIds = cards.map(c => c.id)
    const inZoneIds = this._cards.map(c => c.id)

    if (incomingIds.some(id => inZoneIds.includes(id))) {
      throw new Error('One or more cards passed in already exist in this zone.')
    }

    this._cards.push(...cards)
  }

  removeCards(cards: ICard[]): ICard[] {
    const incomingIds = cards.map(c => c.id)
    const inZoneIds = this._cards.map(c => c.id)

    if (incomingIds.some(id => !inZoneIds.includes(id))) {
      throw new Error('One or more cards passed in do not exist in this zone.')
    }

    const result = this._cards.filter(card => incomingIds.includes(card.id))
    this._cards = this._cards.filter(card => !incomingIds.includes(card.id))

    return result
  }
}

export interface IZone {
  cards: ICard[]
  size: number
  addCards(cards: ICard[]): void
  removeCards(cards: ICard[]): ICard[]
}