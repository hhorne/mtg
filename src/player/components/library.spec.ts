import Library from './library'
import { ICard } from '../../card';
import { createCards, createPlayer } from './shared/test-helpers'

describe('Library', () => {

	it('initializes cards based on player.deck', () => {
		const deck: ICard[] = createCards(10)
		const player = createPlayer()
		const library = new Library(player, deck)

		// Not the same reference anymore, so this may not be an
		// appropriatetest. Rethinking.
		// const match = library.cards === deck

		// expect(match).toBeTruthy()
	})

	describe('has cards', () => {

		describe('draw()', () => {

			it('removes the first card from the library', () => {
				const deck: ICard[] = createCards(10)
				const player = createPlayer()
				const library = new Library(player, deck)
				const topCard = library.cards[0]

				const card = library.draw()
				const drewTopCard = topCard === card

				expect(drewTopCard).toBeTruthy()
			})

		})

	})

})