import Guid from '../../../shared/guid'
import { ICard } from '../../../card'
import { IPlayer } from '../..'
import { IZone } from '../zone';

export const createCards = (num: number): ICard[] =>
	Array.from(new Array(num), (val, index) => index)
	.map(n => ({
		id: Guid.create(),
		name: `Card ${n + 1}`,
		types: []
	}))

export const createZone = (cards: ICard[] = []): IZone => ({
	cards,
	size: cards.length,
	addCards: (cards: ICard[]) => {},
	removeCards: (cards: ICard[]): ICard[] => []
})

export const createPlayer = ({
	id = 'playerid',
	name = 'player-name',
	battlefield = createZone(),
	exile = createZone(),
	graveyard = createZone(),
	hand = createZone(),
	library = createZone(),
} = {}): IPlayer => ({
	id,
	name,
	battlefield,
	exile,
	graveyard,
	hand,
	library,
})