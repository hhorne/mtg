import { IZone } from './components/zone'
import { LifeCounter, ManaPool } from './components'

export { default } from './player'
export { default as Defaults } from './defaults'

export interface IPlayer {
  id: string
  name: string
  battlefield: IZone
  exile: IZone
  graveyard: IZone
  hand: IZone
  library: IZone
  // lifeCounter: LifeCounter
  // manaPool: ManaPool
}