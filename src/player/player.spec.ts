import Player from './player'
import { Battlefield, Exile, Graveyard, Hand, Library, LifeCounter, ManaPool, PlayLand } from './components'
import { createCards } from './components/shared/test-helpers';

describe('Player', () => {

  it('has a Battlefield', () => {
    const player = new Player('playerid', 'test', [])

    const { battlefield } = player

    expect(battlefield).toBeInstanceOf(Battlefield)
  })

  it('has a Exile', () => {
    const player = new Player('playerid', 'test', [])

    const { exile } = player

    expect(exile).toBeInstanceOf(Exile)
  })

  it('has a Graveyard', () => {
    const player = new Player('playerid', 'test', [])

    const { graveyard } = player

    expect(graveyard).toBeInstanceOf(Graveyard)
  })

  it('has a Hand', () => {
    const player = new Player('playerid', 'test', [])

    const { hand } = player

    expect(hand).toBeInstanceOf(Hand)
  })

  it('has a Library', () => {
    const player = new Player('playerid', 'test', [])

    const { library } = player

    expect(library).toBeInstanceOf(Library)
  })

  it('has a Life Counter', () => {
    const player = new Player('playerid', 'test', [])

    const { lifeCounter } = player

    expect(lifeCounter).toBeInstanceOf(LifeCounter)
  })

  it('has a Mana Pool', () => {
    const player = new Player('playerid', 'test', [])

    const { manaPool } = player

    expect(manaPool).toBeInstanceOf(ManaPool)
  })

  it('can Play Land', () => {
    const player = new Player('playerid', 'test', [])

    const playLand = player.getComponent(PlayLand.id)

    expect(playLand).toBeInstanceOf(PlayLand)
  })

  it('initializes its library from the deck', () => {
    const deck = createCards(5)
    const player = new Player('playerid', 'name', deck)
    const { library } = player
    const deckCardIds = deck.map(c => c.id)
    const libraryCardIds = library.cards.map(c => c.id)

    const allFound = deckCardIds.every(id => libraryCardIds.includes(id))

    expect(allFound).toBeTruthy()
  })

})