import { IPlayer } from '.'
import { ICard } from '../card'
import Entity from '../shared/entity'
import {
  Battlefield,
  Exile,
  Graveyard,
  Hand,
  Library,
  LifeCounter,
  ManaPool,
  PlayLand,
} from './components'

export default class Player extends Entity implements IPlayer {
  readonly id: string
  readonly name: string
  readonly deck: ICard[]

  constructor(id: string, name: string, deck: ICard[]) {
    super([
      { type: Battlefield },
      { type: Exile },
      { type: Graveyard },
      { type: Hand },
      { type: Library, args: [[...deck]] },
      { type: LifeCounter },
      { type: ManaPool },
      { type: PlayLand },
    ])

    this.id = id
    this.name = name
    this.deck = deck
  }

  get battlefield() {
    return this.getComponent<Battlefield>(Battlefield.id)
  }

  get exile() {
    return this.getComponent<Exile>(Exile.id)
  }

  get graveyard() {
    return this.getComponent<Graveyard>(Graveyard.id)
  }

  get hand() {
    return this.getComponent<Hand>(Hand.id)
  }

  get library() {
    return this.getComponent<Library>(Library.id)
  }

  get lifeCounter() {
    return this.getComponent<LifeCounter>(LifeCounter.id)
  }

  get manaPool() {
    return this.getComponent<ManaPool>(ManaPool.id)
  }
}