import { ICard } from '.';
import Guid from '../shared/guid'
import { ComponentInitializer } from '../shared/component'
import Entity from '../shared/entity'

export default class Card extends Entity implements ICard {
  id: Guid
  name: string = ''
  types: string[] = []

  constructor({ id, name, types, components }: CardConstructorProps) {
    super(components)
    this.id = id || Guid.create()
    this.name = name
    this.types = types
  }
}

interface CardConstructorProps {
  id?: Guid
  components?: ComponentInitializer[]
  name: string
  types: string[]
}