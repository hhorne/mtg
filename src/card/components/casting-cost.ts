import Card from '../card'
import Component from '../../shared/component'
import Mana from '../../mana'

export default class CastingCost implements Component {
  static id: string = 'Casting-Cost'
  private _card: Card
  private _cost: Array<string>

  constructor(card: Card, cost: Array<Mana> = []) {
    this._card = card
    this._cost = cost
  }

  get id(): string {
    return this.id
  }

  get cost(): Array<string> {
    return this._cost
  }

  get convertedManaCost(): number {
    return this._cost.length
  }
}