import Card from '../card'
import Component from '../../shared/component'

export default class Tappable implements Component {
  static id: string = 'Tappable'
  private card: Card
  private tapped: boolean = false

  constructor(card: Card, tapped: boolean = false) {
    this.card = card
    this.tapped = tapped
  }

  get id(): string {
    return this.id
  }

  get isTapped(): boolean {
    return this.tapped
  }

  tap(): boolean {
    if (this.isTapped) {
      return false
    }

    this.tapped = true
    return this.tapped
  }

  untap(): boolean {
    if (this.isTapped) {
      this.tapped = false
      return true
    }

    return false
  }
}