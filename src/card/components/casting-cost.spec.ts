import CastingCost from './casting-cost'
import Card from '../card'

describe('CastingCost', () => {

  it('defaults to a 0 mana casting cost', () => {
    const card = new Card({ name: 'test card', types: [] })
    const castingCost = new CastingCost(card)
    
    expect(castingCost.cost.length).toBe(0)
  })

})