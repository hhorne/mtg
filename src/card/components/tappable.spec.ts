import Card from '../card'
import Tappable from './tappable'

describe('Tappable', () => {

  it('defaults to untapped', () => {
    const card = new Card({ name: 'test card', types: [] })
    const component = new Tappable(card)

    const isTapped = component.isTapped

    expect(isTapped).toBeFalsy()
  })

  it('can be created tapped', () => {
    const card = new Card({ name: 'test card', types: [] })
    const tapped = true
    const component = new Tappable(card, tapped)

    const isTapped = component.isTapped

    expect(isTapped).toBeTruthy()
  })

  describe('tap()', () => {

    it('toggles isTapped to false and returns true when card is untapped', () => {
      const card = new Card({ name: 'test card', types: [] })
      const component = new Tappable(card)

      const tapResult = component.tap()

      expect(tapResult).toBeTruthy()
      expect(component.isTapped).toBeTruthy()
    })

    it('returns false when card is already tapped', () => {
      const card = new Card({ name: 'test card', types: [] })
      const tapped = true
      const component = new Tappable(card, tapped)

      const tapResult = component.tap()

      expect(tapResult).toBeFalsy()
      expect(component.isTapped).toBeTruthy()
    })

  })

  describe('untap()', () => {

    it('toggles isTapped to false and returns true when card is tapped', () => {
      const card = new Card({ name: 'test card', types: [] })
      const tapped = true
      const component = new Tappable(card, tapped)

      const untapResult: boolean = component.untap()

      expect(untapResult).toBeTruthy()
      expect(component.isTapped).toBeFalsy()
    })

    it('returns false when card is already untapped', () => {
      const card = new Card({ name: 'test card', types: [] })
      const component = new Tappable(card)

      const untapResult: boolean = component.untap()

      expect(untapResult).toBeFalsy()
      expect(component.isTapped).toBeFalsy()
    })

  })

})