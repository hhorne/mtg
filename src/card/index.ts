import Guid from '../shared/guid'
import Component from '../shared/component'

export { default } from './card'
export { default as Types } from './types'

export interface ICard {
  id: Guid
  components?: Map<string, Component>
  name: string
  types: string[]
}