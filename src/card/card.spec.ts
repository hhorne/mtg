import Card from './card'
import CardTypes from './types'
import Tappable from './components/tappable';
import { ComponentInitializer } from '../shared/component';

describe('Card', () => {

  it('accepts constructor params', () => {
    const id = 'expected'
    const name = 'test!'
    const types = [CardTypes.BasicLand.id]
    const card = new Card({ id, name, types })
  
    expect(card.id).toBe(id)
    expect(card.name).toBe(name)
    expect(card.types.length).toBe(1)
    expect(card.types[0]).toBe(CardTypes.BasicLand.id)
  })

  it('fills in its id property when none is provided', () => {
    const name = 'test!'
    const types = [CardTypes.BasicLand.id]
    const card = new Card({ name, types })

    expect(card.id).toBeTruthy()
  })

  describe('getComponent', () => {

    it('returns a Component when passed a matching componentId', () => {
      const name = 'test!'
      const types = [CardTypes.BasicLand.id]
      const components: ComponentInitializer[] = [{ type: Tappable }]
      const card = new Card({ name, types, components })
      const tapComponent = card.getComponent(Tappable.id) as Tappable

      expect(card.components.size).toBe(1)
      expect(tapComponent).toBeDefined()
      expect(tapComponent.isTapped).toBeFalsy()
      expect(tapComponent.tap()).toBeTruthy()
      expect(tapComponent.isTapped).toBeTruthy()
    })

    it('returns undefined when passed an unrecognized componentId', () => {
      const name = 'test!'
      const types = [CardTypes.BasicLand.id]
      const card = new Card({ name, types })

      const registerResult = card.registerComponent(Tappable)
      const tapComponent = card.getComponent(Tappable.id) as Tappable

      expect(registerResult).toBe(true)
      expect(card.components.size).toBe(1)
      expect(tapComponent).toBeDefined()
      expect(tapComponent.isTapped).toBeFalsy()
    })

  })

  describe('registerComponent', () => {

    it('adds to the component map', () => {
      const name = 'test!'
      const types = [CardTypes.BasicLand.id]
      const card = new Card({ name, types })

      const registerResult = card.registerComponent(Tappable)
      const tapComponent = card.getComponent(Tappable.id) as Tappable

      expect(registerResult).toBe(true)
      expect(card.components.size).toBe(1)
      expect(tapComponent).toBeDefined()
      expect(tapComponent.isTapped).toBeFalsy()
      expect(tapComponent.tap()).toBeTruthy()
      expect(tapComponent.isTapped).toBeTruthy()
    })

    it('passes arguments to the component', () => {
      const name = 'test!'
      const types = [CardTypes.BasicLand.id]
      const card = new Card({ name, types })
      const tapped = true

      const registerResult = card.registerComponent(Tappable, tapped)
      const tapComponent = card.getComponent(Tappable.id) as Tappable

      expect(registerResult).toBe(true)
      expect(card.components.size).toBe(1)
      expect(tapComponent).toBeDefined()
      expect(tapComponent.isTapped).toBeTruthy()
    })

  })

})
