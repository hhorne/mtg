export default {
  BasicLand: {
    id: 'basic-land',
    name: 'Land',
  },
  Land: {
    id: 'land',
    name: 'Land',
  },
  Instant: {
    id: 'instant',
    name: 'Instant',
  },
  Sorcery: {
    id: 'sorcery',
    name: 'sorcery',
  },
  Enchantment: {
    id: 'enchantment',
    name: 'Enchantment',
  },
  Creature: {
    id: 'creature',
    name: 'creature',
  },
  Artifact: {
    id: 'artifact',
    name: 'Artifact',
  },
}